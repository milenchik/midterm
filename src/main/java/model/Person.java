package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="person")

public class Person {
@Id
private long id;
private String firstName;
private String lastName;
private String city;
private String phone;
private String telegram;

}
