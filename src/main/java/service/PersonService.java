package service;

import model.Person;
import org.springframework.stereotype.Service;
import repository.PersonRepository;

import java.util.List;

@Service
public class PersonService {
private final PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> getAll() {
     return (List<Person>) personRepository.findAll();
    }

    public void deleteById(long id) {
        personRepository.deleteById(id);
    }

    public Person create(Person person) {
        return personRepository.save(person);
    }
}
