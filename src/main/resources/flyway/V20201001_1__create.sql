CREATE TABLE  if not exists Person
(
    id        serial,
    firstName varchar(255),
    lastName  varchar(255),
    phone     varchar(255),
    city      varchar(255),
    telegram  varchar(255),
    PRIMARY KEY (ID)
);